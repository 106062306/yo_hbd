# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : [Forum]
* Key functions (add/delete)
    1. [sign up, in]
    2. [firebase deploy]
    3. [read/write data]
    4. [RWD]
    5. [user page]
    6. [post page, post list page, leave comment under any post]
    7. [sign up/in with Google or other third-party accounts]
    8. [add Chrome notification]
    9. [CSS animation]

    
* Other functions (add/delete)
    1. [remove article that posted by the same user]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：[https://ssmid-c98d2.firebaseapp.com/]

# Components Description : 
1. [sign up, in]: 連結firebase來儲存user的email and password
2. [firebase deploy]: use firebase init and firebase deploy
3. [read/write data]: 用firebase的realtime database來儲存並可讀資料(user page可顯示po過的文章)
4. [RWD]: use bootstrap, 視窗縮小時navbar變成下拉式選單, 'Choose a topic', 'Speak Out Your Idea', 'Discuss With Others' 縮小後這三個由一排三個變成一排一個
5. [user page] : 顯示使用者的email和po過的文章
6. [post page, post list page, leave comment under any post]: 在初始頁面能看到所有topic, 點navbar上的topic, 畫面可到那個topic的<div>位置, 可以選擇其一並進入該板看文章, 最下方可以發布文章(title, content), 由文章發布的先後顯示Article1, 2, 3, 4..., 每篇文章都可以留言(click comment button), 每篇文章、每則留言都會顯示是哪個user留的
7. [sign up/in with Google or other third-party accounts] : 用firebase的功能連結gmail帳號
8. [add Chrome notification]: 在navbar有個小鈴鐺, 點選可以允許跳出通知, 之後發一篇文章(click submit button)時螢幕右下角會有通知
9. [CSS animation] : hover在navbar上有動畫

# Other Functions Description(1~10%) : 
1. [remove article that posted by the same user]: 在user page可看到自己發過的文章並可刪除
