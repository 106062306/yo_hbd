var url = location.href;
var dataSent = url.split("?");
var article = dataSent[1];
function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a class='dropdown-item' href='profile.html'>" + user.email + "</a><a class='dropdown-item' id='logout-btn' href='signin.html'>Logout</a>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btn = document.getElementById('logout-btn');
            btn.addEventListener('click', function(){
                firebase.auth().signOut().then(function() {
                    console.log('Signed Out');
                  }, function(error) {
                    console.error('Sign Out Error', error);
                  });
            })
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('comment_list').innerHTML = "";
        }
    });

    comment_btn = document.getElementById('comment_btn');
    comment_txt = document.getElementById('comment');
    var commentsRef = firebase.database().ref("world_list/" + article);

    comment_btn.addEventListener('click', function () {
        if (comment_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            var user = firebase.auth().currentUser;
            var new_post = {
                email: user.email,
                comment : comment_txt.value
            }
            commentsRef.push(new_post);
            comment_txt.value = "";
        }
    });

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'>";
    var str_middle = "<div class='media text-muted pt-3'><img src='./images/content.png' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray' id='value'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    var list_ = document.getElementById("article_list");
    commentsRef.once('value')
        .then(function (snapshot) {
            list_.innerHTML += `<h1><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val().title + `</h1><br>`;
            list_.innerHTML += `<h1><img src='./images/content.png' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val().comment + `</h1>`;
            list_.innerHTML += `<hr class="featurette-divider">`;
        })
        .catch(e => console.log(e.message));
    var list = document.getElementById("comment_list");
    commentsRef.once('value')
        .then(function (snapshot) {
            list.innerHTML = "";
            var j = 1;
            for(let i in snapshot.val())
            {
                if(i == 'title' || i == 'email' || i == 'comment') continue;
                total_post.push(snapshot.val()[i]);
                list.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id=""><img src='./images/content_.png' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>Comment `+ j + `</h3><br><br>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>message from: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                list.innerHTML += `<hr class="featurette-divider">`;
                j++;
            }
        })
        .catch(e => console.log(e.message));
    commentsRef.on('value', function (snapshot) {
        list.innerHTML = "";
        var k = 1;
        for(let i in snapshot.val())
        {
            if(i == 'title' || i == 'email' || i == 'comment') continue;
            total_post.push(snapshot.val()[i]);
            list.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id=""><img src='./images/content_.png' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>Comment `+ k + `</h3><br><br>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>message from: ` + snapshot.val()[i].email + `</p>` + str_after_content;
            list.innerHTML += `<hr class="featurette-divider">`;
            k++;
        }
    })
}

window.onload = function () {
    init();
};