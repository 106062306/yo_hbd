function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btn = document.getElementById('logout-btn');
            btn.addEventListener('click', function(){
                firebase.auth().signOut().then(function() {
                    console.log('Signed Out');
                  }, function(error) {
                    console.error('Sign Out Error', error);
                  });
            })
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_title = document.getElementById('title');
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    var postsRef = firebase.database().ref("tech_list");

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            var user = firebase.auth().currentUser;
            var new_post = {
                email: user.email,
                title: post_title.value,
                comment : post_txt.value
            }
            postsRef.push(new_post);
            post_title.value = "";
            post_txt.value = "";
        }
    });

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'>";
    var str_middle = "<div class='media text-muted pt-3'><img src='./images/content.png' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray' id='value'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    //var postsRef = firebase.database().ref('world_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    var list = document.getElementById("post_list");
    postsRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            list.innerHTML = "";
            var j = 1;
            for(let i in snapshot.val())
            {
                total_post.push(snapshot.val()[i]);
                list.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ j + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title +  `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
                list.innerHTML += `<button id="comment_b" onclick="giveValue('` + i + `')">comment</button>`;
                list.innerHTML += `<hr class="featurette-divider">`;
                j++;
            }
        })
        .catch(e => console.log(e.message));
    postsRef.on('value', function (snapshot) {
        list.innerHTML = "";
        var k = 1;
        for(let i in snapshot.val())
        {
            total_post.push(snapshot.val()[i]);
            list.innerHTML += str_before_username + `<h3 class="border-bottom border-gray pb-2 mb-0" id="article">Article `+ k + `</h3><br><br>` + `<h4><img src='./images/topic.jpg' alt='' class='mr-2 rounded-corner' style='height:32px; width:32px;'>` + snapshot.val()[i].title +  `</h4>` + str_middle + snapshot.val()[i].comment + `<p id='email'><br><br>author: ` + snapshot.val()[i].email + `</p>` + str_after_content;
            list.innerHTML += `<button id="comment_b" onclick="giveValue('` + i + `')">comment</button>`;
            console.log(i);
            list.innerHTML += `<hr class="featurette-divider">`;
            k++;
        }
    })

    var database = firebase.database().ref("tech_list");    

    var notify = document.getElementById('notify');
    notify.addEventListener('click', function(e) {
        e.preventDefault();
        if(!window.Notification) {
            console.log('No supported');
        }
        else {
            Notification.requestPermission()
            .then(function(p) {
                if(p == 'denied') console.log('You denied to show notification');
                else if(p == 'granted') console.log('You allowed to show notification');
            })
        }
    })

    database.on('child_added', function(data) {
        if(Notification.permission !== 'default') {
            var notify;
            notify = new Notification("New message from " + data.val().email,{
                'email': data.val().email,
                'title': data.val().title,
                'content': data.val().comment
            });
            notify.onclick = function() {
                console.log(this.content);
            }
        }
        else {
            console.log('Please allow the notification first');
        }
    });
}

function giveValue(n) {
    console.log("here");
    window.location.href = 'tech_comment.html?' + n;
}
window.onload = function () {
    init();
};